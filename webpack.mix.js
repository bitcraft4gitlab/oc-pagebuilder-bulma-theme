let mix = require('laravel-mix');
require('laravel-mix-clean-css');
const s3Plugin = require('webpack-s3-plugin');

let webpackPlugins = [];
if (mix.inProduction() && process.env.UPLOAD_S3) {
    webpackPlugins = [
        new s3Plugin({
            include: /.*\.(css|js|xsl)$/,
            s3Options: {
                accessKeyId: '<s3-user>',
                secretAccessKey: '<s3-key>',
                region: 'eu-central-1',
            },
            s3UploadOptions: {
                Bucket:'<s3-bucket>',
                CacheControl: 'public, max-age=31536000'
            },
            basePath: 'app',
            directory: 'assets'
        })
    ]
}

mix.options({
    processCssUrls: false,
    autoprefixer: {
        enabled: true,
        options: {
            overrideBrowserslist: ['last 2 versions', '> 1%'],
            cascade: true,
            grid: true,
        }
    },
})
    .setPublicPath('/')
    .sass('assets/scss/styles.scss', 'assets/css/styles.css')
    .cleanCss({
        level: 2,
        format: mix.inProduction() ? false : 'beautify' // Beautify only in dev mode
    })
    .combine(['assets/css/styles.css', 'assets/css/framework.extras.css'], 'assets/css/app.css')
    .js(['assets/js/app.js', 'assets/js/framework.combined-min.js'], 'assets/js/all.js')
    .minify('assets/js/all.js');

mix.webpackConfig({
    plugins: webpackPlugins
});

if (mix.inProduction()) {
    mix.version();
}
